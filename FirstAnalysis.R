dat <- read.csv('training.csv', col.names=1:15)
pairs(dat, pch='.')

lm.fit <- lm(X15 ~ ., dat)
summary(lm.fit)

predict(lm.fit, dat[8,1:14]) # multiplies the weights (fitted by lm.fit model) with
# the data vector dat[8,1:14] - the 8th row of the data matrix (just for example)

n <- dim(dat)[1]
p <- dim(dat)[2] - 1
errors <- numeric(n)

loo.cv <- function(dat) {
  for (i in 1:n) {
    fit <- lm(X15 ~ ., dat[-i, ])
    prediction <- predict(fit, dat[i, 1:14])
    errors[i] <- (dat[i, 15] - prediction)^2
  }
  print(errors)
  mean(errors)
}

cv <- (loo.cv(dat))


ss.tot <- (n - 1) * (sd(dat$X15)^2)
ss.res <- cv
r.squared <- 1 - (ss.res/ss.tot)
(r.squared)

# check out means of features and their sd's

apply(dat, 2, mean)
apply(dat, 2, sd)

# OWAIT X8 is a factor, check this shit out

x8 <- as.factor(dat$X8)
levels(x8)

# looks like all of them are stepwize, checkin'

for (i in 1:p) {
  print(levels(as.factor(dat[, i])))
}

# CONFIRMED there is no noise in predictor variables, only multipliers of N

# DECISION: put all features into the range of [0, 1]

dat.sd <- dat

for (i in 1:p) {
  dat.sd[, i] <- dat[, i] / max(dat[, i])
}

cv <- (loo.cv(dat.sd))
(cv)


# Transform Y -> log(Y)

dat.tr <- dat.sd
dat.tr$X15 <- log(dat.tr$X15)
(cv <- loo.cv(dat.tr))

ss.tot <- (n - 1) * (sd(dat.tr$X15)^2)
ss.res <- cv
r.squared <- 1 - (ss.res/ss.tot)
(r.squared)
