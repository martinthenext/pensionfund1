source('Modular.R')

feature.transform <- function(dat) {
  x15.idx <- which(names(dat)=='X15')
  feature.idx <- setdiff(1:ncol(dat), x15.idx)
  for (i in feature.idx) {
    dat[, i] <- dat[, i] / max(dat.orig[, i])
  }
  # X13 -> factor
  dat$fX13 <- as.factor(dat$X13 > 0.4)
  dat$X14 <- dat$X14
  dat$X14sq <- (dat$X14)^2
  dat$fX3 <- as.factor(dat$X3 > 0.2) 
  dat$fX10 <- as.factor(dat$X10 < 0.4) 
  return(dat)
}

train.model <- function(dat) {
  rownames(dat) <- NULL
  dat <- feature.transform(dat)
  dat$trX15 <- response.transform(dat$X15)
  fit <- lm(trX15 ~ fX3 + X5 + X8 + fX10 + fX13 + X14 + fX13*X14 + X12*X14 + X5*fX13 + X14sq, data=dat)
  return(fit)
}

# import dataset from RStudio -> validation

colnames(validation) <- paste('X',1:14, sep="")

model <- train.model(dat.orig)
result <- predict.from.model(model, validation)
write.csv(result, file="pension1.csv", row.names=FALSE)
